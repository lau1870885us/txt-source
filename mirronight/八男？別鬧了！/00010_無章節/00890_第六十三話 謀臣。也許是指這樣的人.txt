 謀臣。也許是指這樣的人

「嗯ーーー」

在鮑麥斯特騎士爵領發生了叛亂，在沒有任何犧牲者之下被鎮壓了。

結果主謀克勞斯高明地避開了被絞首，似乎如他所願，但總覺得鬆了一口氣也說不定。

果然，不想看到處刑人的真實情況上演。

「威爾，克勞斯先生抱有複雜的情感呢」

叛亂的鎮壓也平安無事地結束了，大家奮鬥地進行事後的處理，我一個人領地盡頭處的草原上隨意地躺了下來望著天空。

護衛只有伊娜一個人，我想起不了多大的作用。

因為，我要求用她的大腿當膝枕了以致無法動彈。

這個地方野生動物非常少來並沒有太大的問題，身為這種身份在稍遠處還有幾名保護我的士兵在。

高權重，可說是相當夠嗆的。

「該說是複雜嗎，應該說是無言以對」(ギャフン，這個字有看笨蛋測驗招喚獸時吉井明久很常用，除了指放棄，還有無言以對的意思。這邊放哪個都很怪就是了)

「無言以對……。已經不是孩子了」

以救援赫爾曼哥哥他們為目的衝進食堂後，我對著克勞斯詢問『那麼討厭絞首嗎？』了。

原本日本人的感受打從心底的絞首期望也沒了，我想說出那些話時克勞斯也動搖了吧。

結果很漂亮，虛張聲勢的事卻被看穿了。

「克勞斯先生似乎變了，好像也沒有將情感隱藏起來，我認為(他的)心情很複雜」

儘管如此，心裡面也會煩惱、震驚、生氣。

「他孫子們的愚蠢程度，我想才會嘆氣吧」

現在是被軟禁的狀態，已經預訂從明天起被送到新村落建設預定地那。

人數有，瓦爾特和卡爾以及參加叛亂的年輕領民們和他們的妻子與孩子們共計二十二名。

首先要從自己住的房子建設起，然後自食其力進行開墾。

以前的田地和家則要被沒收，建立新的家所需的資材由赫爾曼哥哥準備。

以前家中所擁有的傢俱與衣服和現金等所有權是被承認的，理由是如果連那些都不給的話是連食物都買不起的。

還有人頭稅，栽培作物是要繳稅的，這些決定三年內給予免除。

只是，雖然三年內的稅被免除了但吃的部分要是自己的田地不進行種植就會餓死，所以克勞斯為了貼補家用需要去打工。

總之，他原本的家和闐地也被沒收了。

金錢之類的財產，聽說克勞斯並沒有很多。

大概就是一般里長的平均值左右吧。

如果，欺騙父親代為繳稅就有再次面臨絞首的可能性，但是似乎沒有這麼做。

(注1:チョロまかして，欺騙、矇騙、偷竊的意思。前面的チョロ猜測是チョロイ，粗心大意的意思。似乎作者把兩個單字並在一起用。所以那句翻譯另一種型態是:利用父親的粗心大意進行欺騙代為繳稅的話....)

(注2:縛り首，絞首跟絞刑是不同的，前者在日本戰國時代吊死後頭會被砍下來，後者則無。故事內應該是用戰國時代這種比較殘忍的方式，所以主角才會比較抗拒)

稍微有點可惜，我想克勞斯也應該不會做傻事。

跟著我來的克勞斯沒有攜帶太多的現金，少許的衣物和隨身物品全都只放進一個包包裡。

『超過六十歲，能到外頭工作。相當興奮呢』

再加上，打從心裡對目前的情況很期待，克勞斯的膽量相當大吧。

「克勞斯先生，他的目的應該達到了吧」

要是在那種狀態下置之不理，克勞斯死後不能否定瓦爾特和卡爾不會暴走，所以給他們嚴厲的懲罰，採取不殺的方式結束事情才是好的對策。

那個，才是叛亂的目的吧。

從零開始的開墾很辛勞瓦爾特和卡爾一副可憐的樣子，因為自作自受更沒辦法說出口。

「開發很困難，監視的眼線也是要有的」

必要的物品在商店就能買的到，禁止經營新商店的萊納廉售，不過高價販賣也是被禁止的這是赫爾曼哥哥所下達的命令。

還有，附帶二十四小時輪替的監視人員。

『下次再有什麼企圖，就由我來負責處理。會交由威爾再次進行鎮壓。因為這裡是我的領地』

有這樣的交換(條件)，他們老實地接受處罰了。

沒有絞首太天真了這樣的意見也有出現，因為要靠自己的力量在什麼都沒有的草原上開墾到死，所以某種意義上是比絞首還嚴厲的處罰吧。

連逃走都不可能。

有進行監視，雖然說開發開始了但他們開墾的草原周圍還是有許多凶暴的野生動物。

似乎連弓都不會用的瓦爾特和卡爾，要逃走是不可能的吧。

「那些人交給赫爾曼哥哥，我們要去打仗了」

「威爾，越來越像貴族了呢」

「伊娜，也越來越像太太了嗎？」

「沒有自信……」

「所有人都同樣無聊著，那樣子比較好不是嗎？

我很感謝能和伊娜在一般事情上很談得來」

埃莉絲稍微有些拘謹，露易絲很有趣但卻有很奇怪的時候，維爾瑪偶爾會很毒舌，卡特莉娜怎麼說呢有時會覺得想法不一致。

所以，我認為很談得來的伊娜對我來說是很珍貴的。

「正因為如此，現在就像普通的未婚夫妻一樣繼續膝枕吧」(腳不會酸嗎...)

「是呢。我的腳不會太結實嗎？」(私の足って硬くない，一句話問兩種意思。另一句翻譯是:我不會太嚴肅、不滿足嗎)

「會嗎？並沒有很硬喔」

「那真是太好了」

我在伊娜的膝枕短短的睡了約一個小時左右。

「鮑邁斯特伯爵大人，接您來了」

「辛苦了。果然，這種時候是小型的魔導飛行船方便啊」

鎮壓叛亂之後處理完畢的第二天，我們迎來了一艘小型的魔導飛行船。

在這個最重要的時期腳步拉長了在東部彙集對著布羅瓦邊境伯來一擊嗎。

這樣是不預先又是什麼？企圖，也有可能，再加上在南部的開始大規模出兵也做著之類的，布萊希萊德『ブライヒレーダー=百度翻譯（胸罩イヒ雷達）』邊境伯也參軍隊要求出來了。

偏向父母的請求儘可能採納，取而代之的是與此對應。謀求方便獲得是正確的關係。

回報的也是有必要的，所以我軍的移動小型魔導飛行船、持有阿姆斯特朗導師的次子亨利克開始出租運送的享受的事情了。

暫時的，租船合約。

「明明是掙錢的時候，實在是很抱歉啊。」

「不，利潤足夠了。那麼，人數按照預定嗎？」

參加者，我們布蘭塔庫先生，還有鮑邁斯特伯爵家諸侯軍隊那個在モーリッツ率領的佈陣。

「草！我也兵出想了！」

赫爾曼哥哥也想著派出少數的士兵，但沒有犧牲的內亂才結束，正是忙亂的時候，帶著懊悔的表情向我傳達不參與的事項。

「唯一參加的是克勞斯。」

「那小子，全部計算的？」

「我最近也不清楚」

之後，從名主引退成為自由職業者的克勞斯，抓住了叛亂失敗的布羅瓦方的士兵們的身影。。

「那樣的載著沒問題嗎？」

「克勞斯漂亮地說服了。」

「那個人，可是叛亂的主謀者啊？」

臉引き攣ら讓父親是不像有常識的人的亨利克滅亡了，實際上克勞斯那伙寢返ら了是事實。

「館様是，打算把他們當自己的棋子使用嗎？」

臨時決定僱傭克勞斯之後，他就有了抓到叛亂分子的用途。

「打算這樣做，但首先向布萊希萊德邊境伯問問看吧」

「毫無疑問的，布萊希萊德邊境伯是什麼也不會說就發出許可的。因為抓住了他們的是館様。處置他們的權利，除了館様沒有別人有。

無論父母和孩子的關係有多麼親近，但是邊境布萊希萊德邊境伯家和鮑邁斯特伯爵家是不同的家族。

所以，俘虜的處理和戰利品的處理是要講清的。克勞斯這麼說明了。

「因此，提前進行合適的說服嗎？。我，勸說試試吧」

「克勞斯是嗎？作為背叛者正在處理一片噓聲不是嗎。」

「在那裡逆轉的，可就是老人家的智慧咯"

拿出許可，一起去他們被抓住的空房子，打開了門。從他們預料那樣發生了巨大的噓聲。

「叛徒！」

「我們被賣了！」

主謀者克勞斯，為何和我站在一起。

他們的憤怒是理所當然，到了這種地步我也想支持他們。

「可沒有那樣的誤會啊，我說一下，我可是全力協助叛亂了哦。」

克勞斯，正因為那樣鮮明的領主的館落了的說。

只是，以運氣不佳，來鎮壓對方的失敗。

或者說，在我面前滿不在乎的說自己全力參加了叛亂。克勞斯真是有著很好的性格。

「鮑邁斯特伯爵大人，布萊希萊德邊境伯大人的平衡ターク大人，西部的地位和評價像狂飆一樣。三人在區域坦被壓制下去……。我發誓勾結之類沒發生，那樣的東西鮑邁斯特伯爵大人不是必要的。」

聽著克勞斯的說明，逐漸噓聲的聲音停止了。

「那，被抓住的我們，會發生什麼事情？」

他們的領導人的，三十歲左右的男性向克勞斯提問的。

「這是。。托馬斯殿。大家的的仕途的中間人？」（それはですね。トーマス殿。みなさんの仕官の口利きです不懂啥意思）

「絞首。我們嗎？開什麼玩笑？」

「不，不是開玩笑哦。」

克勞斯的說明還在持續著。

「迪巴爵士伯爵家，斯文デリン先生個人才能獨自開闢的御家。好的事有，不過，果然還是人手不夠。普通的話，以托馬斯殿所犯的罪，絞刑是很妥當的。但是，在這裡隨便將人殺死的話，很可惜。」

「人的話，多餘的吧。在王都的貧民窟都可以聚集到相當多的數量吧。」

「這樣的開拓民，是鮑邁斯特伯爵家變得能在出任文官武官的被教育過的人是必要的。托馬斯殿一樣，布羅瓦邊境伯領地內的受教育者們。」

克勞斯的發言，包括托馬斯在內的全員移開視線。

他們，是布羅瓦邊境伯家出身的。

「至此，出身隱藏起來的必要也是。以前對我講了吧」

克勞斯很有經驗，所以在不知不覺中就說了吧。

說不定，以自己的女兒作為妾嫁出和孫子們的不走運等的話題，很好地利用了同情心，悄無聲息的打聽到了也說不定。

果然，這傢伙果真的是個老奸巨猾的傢伙。

「你父母家和家人不想給別人添麻煩。那個心情是好的。但是，把托馬斯殿當作一次性使用的又是誰？在這樣的敵區的正中央，在出兵的諸侯部隊中得到的，沒有被作為隨意捨棄扔掉的棋子使用在後方動亂的任務了。我，和你們在一起行動著。ツテ，只要有好好仕宦的能力。」

「那麼這個世界不是那麼天真的……」

以托馬斯為名的首領，ボツボツ開始說話。

零散臣下之臣家裡的三男以下的等等，最好的也只是被當作隨意棄掉的棋子。

若是天才水平的人的話會有從上面來的拯救，自己可沒有那樣的天分。

長子或有門路的富豪這樣的臣下之臣的孩子能被拯救。

「與那種傢伙相比，顯然是我們比較好吧。然而，再則麼愚蠢也會被優先考慮（被救）。」

領地內的統治是一定程度上被系統化，因為所謂的「鳩鳩」水平的笨蛋也沒有辦法了。

所以說，走後門比什麼都被認為是最優先的。

「鮑邁斯特伯爵家進行開發，特別需要人才的話也是知道的。但是，布盧瓦邊境伯家為首的東部貴族，布萊希萊德邊境伯家被層很。我們追求仕途，還沒到達入口就被駁回了。」

兩家的關係惡劣是真的。

並且，在這二代左右仇恨進一步被放大。

布萊希萊德邊境伯，東部一切的權利沒有轉動的吧。

人才的錄用，東部地區出身的人完全是切割著一樣。

「這樣的話，這是一個機會啊。

「一個機會！你在想些什麼！」

「相遇是鎮壓叛亂，的確是很糟糕。但是，要說是走後門這也是走後門吧。伯爵家的當家大人直接碰面了」

克勞斯過分的言論，除了他以外全員啞口無言。

當然，我也語塞。

「如今是負面的沒錯。然而，在這裡活躍重新來過的如何？你們年輕。足夠重新開始。」

「可是……。那個重做和這個重做……」

雖然說是被捨棄的棋子，但與自己的父母家和原主家反抗的事，或許在戰場上與父母兄弟再會的可能性也是存在的。

因此我和全員都沒有考慮過。

「但是，再對面是你們家族沒有想到是吧？」

「……」

「沈黙，肯定接受。」

他們是布盧瓦家一側的一次性使用的棋子。

所以，現在的時刻能夠有容身之所的人是……

「那樣的人，我們當中是沒有的」。

「布羅瓦家一側，不想讓你們成為交涉的弱點」

布羅瓦邊境伯家方面設計的戰爭已經結束了，當然也沒有誰想繼續下去。

如今是為了確保利益，在水面下談判也應該開始了。

那個交涉中，他們只是一個絆腳石而已。

倒不如全滅，因為想要使那樣亂來的作戰進行下去。

「你父母家，你們作為祭品，被遞給了某種利益。取而代之的是，談判的座位上你們的位置是不存在的……」

「懊悔嗎？」

「……」

「家庭的關係確實是很重要的東西。但是，它是相互存在的事情。一方面這樣但是另一方面只是在利用的家庭關係，是扭曲的，利用家族的緣分等同於欺詐」

克勞斯嚴厲的口吻，使托馬斯們沮喪，整個肩膀都垮了下來。

「因此，在這裡再起吧。反正是，叛亂失敗一度失去了的生命。在鮑邁斯特伯爵家仕官重生嗎？」

「重生的……」

克勞斯的真摯的勸說，他們是以誠懇的表情聽了。

我，克勞斯的傳銷組織的新才能的新發現的。

「怎麼樣呢？」

「我要重新開始！立下戰功來仕宦、結婚成家！」

「我也要做了！我老家的人情早就還清了！」

「我我也！」

一會兒。一個接一個手舉起來，最後被剩下的托馬斯向克勞斯提問。

「我已經三十二歲。還能重新開始嗎？」

「嗯。我等，已經六十歲超過了。儘管如此還能動的，所以托馬斯殿下也還剩三十年以上的時間。」

「我也重新來過！讓父母家的可惡兄長好好看看！」

「真是做了很厲害的事啊」

「克勞斯，年高所以經驗多吧？」

「不論做什麼，交涉的能力都很有用啊。作為商人真是很是羨慕。所以，克勞斯殿是做了什麼？」

「與這裡的人進行了一場人生相談。」

一下子モーリッツ來指揮諸侯軍隊混合的問題爆發，所以那個牽引集體這樣的領導隊長其他的部隊組編等也是交給你了。

而且克勞斯，他和我之間羈絆的一樣比較沉著的角色。

稱職的是明白的，但在那個漂亮的圈套下，我的心情有點複雜。

「沒事吧？」

「又是以惡相向的話，真不愧是那個絞首克勞斯啊。那種白痴的事的話不可能成功」

現在，他們在考慮做新的姓氏。

被老家捨棄的話，就以新的名字作為重生的賀禮。

「親切建議的樣子，其實那些傢伙沒有選擇拒絕的退路吧？」

「這樣的話，絞首了，還是好意的嗎……」

真不愧是，謝絕了仕途艱難活著的人。

在王都不能像犯罪奴隸一樣的對待，管理是很麻煩的。

「我，大家迅速運送的工作，所以……。嗯，只是租船的船長。……」

多少提拔的亨利克在別處，克勞斯是自由動著。

「大家都，獨身對吧？」

「老婆和孩子的話，在老家的添麻煩啊」

「這樣的話，這裡是必須堅守的地方。」

「這樣的話？」

「其實這次的出兵延期了，是因為布萊希萊德邊境伯那樣大規模的策劃了相親會」

確實，那是計畫的。

一開始是我的側室闖進會了。但是，我拒絕和埃爾和ローデリヒ把妻子介紹一下會變化。

但是，只有兩個人是希望者們不滿出現，在我家獨身的家臣們要是都能出席的大規模的會更進一步進化的。

「功績給正式仕宦有葉え的話，大家也參加的權利出現您館大人」

「哦——！」

「能結婚！」

「幹勁是進來了！父親和哥哥也在被捉住這樣！」

克勞斯提出來的相親會，使全員都會變得異常的情緒高漲。

「那個……。真的，那樣的許可？」

「克勞斯說，年輕獨身的男人是最有效果」

「原來如此。深啊」

鮑邁斯特騎士爵位領出來一整天，我們乘坐的魔導飛行船到達了布萊希萊德邊境伯軍隊駐紮界線附近的草原上。

